from aws_cdk import (
    aws_iam as iam,
    aws_sqs as sqs,
    aws_sns as sns,
    aws_sns_subscriptions as subs,
    core
)

#Optional artifacts to deploy defined by the boolean flag
#Parameters related to the artifact follows in the same block for easy config

SNS = True
SNS_QueueName = "FormBookitQueue"
SNS_TopicName = "FormBookitTopic"

SES = True #includes a lambda to send messages controlled by backend

S3ForBackup = True

S3ForRelease = True


class FormOptStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        if SNS == True:
            queue = sqs.Queue(
                self, SNS_QueueName,
                visibility_timeout=core.Duration.seconds(300),
            )

            topic = sns.Topic(
                self, SNS_TopicName
            )

            topic.add_subscription(subs.SqsSubscription(queue))

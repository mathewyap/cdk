import json
import pytest

from aws_cdk import core
from form_opt.form_opt_stack import FormOptStack


def get_template():
    app = core.App()
    FormOptStack(app, "form-opt")
    return json.dumps(app.synth().get_stack("form-opt").template)


def test_sqs_queue_created():
    assert("AWS::SQS::Queue" in get_template())


def test_sns_topic_created():
    assert("AWS::SNS::Topic" in get_template())

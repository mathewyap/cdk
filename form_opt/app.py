#!/usr/bin/env python3

from aws_cdk import core

from form_opt.form_opt_stack import FormOptStack


app = core.App()
FormOptStack(app, "form-opt", env={'region': 'us-west-2'})

app.synth()

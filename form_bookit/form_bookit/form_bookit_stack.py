import json
from aws_cdk import (
    aws_ec2 as ec2,
    aws_elasticloadbalancingv2 as elbv2,
    aws_elasticloadbalancingv2_targets as elbv2t,
    core,
)

#default values
vpcId = "vpc-7a9f8a1d"  # Import an Exist VPC
ec2_Type = "t2.micro"
ec2_Name = "Bookit"
ec2_KeyName = "hailyeni"
sslCertArn = "arn:aws:acm:ap-southeast-1:695188266141:certificate/2f0456ab-dbbd-4e55-a615-187951fb83c4"
region = "ap-southeast-1"
purpose = "backup" #new | backup | upgrade

    
class FormBookitStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)
        #essential constructs for the stack including
        #ELB, EC2 and dockers inside
        vpc = ec2.Vpc.from_lookup(self, "VPC", vpc_id=vpcId)

        #Use the AMI created from the running instance
        with open("backup-image.out") as b:
            ami_json = json.load(b)
        linuxAmi = ec2.GenericLinuxImage({
            region : ami_json["ImageId"]
        })

        #Choose the bash script to execute based on purpose 
        if purpose == "upgrade":
            with open("./user_data/upgrade.sh") as f:   #use upgrade instead of generic script
                user_data = f.read()
        else:
            with open("./user_data/user_data.sh") as f:
                user_data = f.read()    

        host = ec2.Instance(self, ec2_Name,
            instance_type=ec2.InstanceType(
                instance_type_identifier=ec2_Type),
            instance_name=ec2_Name+"Server",
            machine_image=linuxAmi,
            vpc=vpc,
            key_name=ec2_KeyName,
            vpc_subnets=ec2.SubnetSelection(
                subnet_type=ec2.SubnetType.PUBLIC),
            user_data=ec2.UserData.custom(user_data) #execute bash script inside the ec2
        )
        # ec2.Instance has no property of BlockDeviceMappings, add via lower layer cdk api:
        host.instance.add_property_override("BlockDeviceMappings", [{
            "DeviceName": "/dev/sda1",
            "Ebs": {
                "VolumeSize": "8",
                "VolumeType": "gp2",
                #"Iops": "100",
                "DeleteOnTermination": "true"
            }
        }, {
            "DeviceName": "/dev/sdb",
            "Ebs": {"VolumeSize": "2",
                "VolumeType": "gp2",
                #"Iops": "100",
                "DeleteOnTermination": "true"
            }
        }
        ])  # by default VolumeType is gp2, VolumeSize 8GB
        host.connections.allow_from_any_ipv4(
            ec2.Port.tcp(22), "Allow ssh from internet")
        host.connections.allow_from_any_ipv4(
            ec2.Port.tcp(80), "Allow http from internet")
        core.CfnOutput(self, "Output", value=host.instance_public_ip)

        if purpose == "backup":
            return

        lb = elbv2.ApplicationLoadBalancer(
            self, ec2_Name+"ELB",
            vpc=vpc,
            internet_facing=True)

        listener = lb.add_listener("Listener",
           certificate_arns=[sslCertArn],
           port=443)
        instanceTarget = elbv2t.InstanceTarget(host, 80)
        listener.add_targets(ec2_Name+"ELBTarget", port=80, targets=[instanceTarget])
        listener.connections.allow_default_port_from_any_ipv4("Open to the world")

        core.CfnOutput(self,"LoadBalancer",export_name="LoadBalancer",value=lb.load_balancer_dns_name)

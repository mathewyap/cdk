#!/bin/bash
if [ ! -f ./backup-image.out ]; then
	aws ec2 create-image --instance-id i-0244c1f6d357f785f --name "backupserver" --no-reboot > backup-image.out
fi
ami_id=`cat backup-image.out | python3 -c "import sys, json; print(json.load(sys.stdin)['ImageId'])"`
echo $ami_id
timeout 5m aws ec2 wait image-available --image-ids "$ami_id" 

if [ $? -eq 0 ]; then
	source .env/bin/activate
	pip install -r requirements.txt
	cdk synth
	cdk deploy --require-approval=never
	cdk diff
	timeout 30m aws ec2 wait instance-running --filters Name=image-id,Values="$ami_id"
	
	if [ $? -eq 0 ]; then
		#echo "Terminating previous instance"
		#aws ec2 terminate-instances --instance_ids `cat last-instance.out` #terminate previous instance seems not needed
		aws ec2 describe-instances --filters Name=image-id,Values="$ami_id" | grep 'InstanceId' | sed 's/\"InstanceId\"\:\s\"//' | sed 's/\",//' > last-instance.out #current instance
		if [ `cat last-instance.out | wc -l` -eq 1 ]; then
			echo "Stopping current instance"
			aws ec2 stop-instances --instance-ids `cat last-instance.out` #stop current instance
			echo "$ami_id" > last-image.out
			rm backup-image.out
			aws ec2 describe-images --image-ids $ami_id | grep 'SnapshotId' | sed 's/\"SnapshotId\"\:\s\"//' | sed 's/\",//' > to_be_removed_snapshots.out
			echo "Deregistering AMI"
			aws ec2 deregister-image --image-id $ami_id
			# remove snapshots linked to image
			input="to_be_removed_snapshots.out"
			while IFS= read -r line
			do
				echo "Deleting snapshot: $line"
				aws ec2 delete-snapshot --snapshot-id $line
			done < "$input"

			echo Backup completed.
			echo Check that housekeeping for previous instance, image and snapshots is done.
		else
			echo New EC2 instance need to be checked. Housekeeping will need to be done for snapshots and image if no longer needed.
		fi
	else
		echo Create Instance failed. Housekeeping will need to be done for snapshots and image if no longer needed.
	fi
else
	echo Create Image failed. Cannot proceed to CDK deployment. 
fi

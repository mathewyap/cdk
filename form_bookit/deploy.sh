#!/bin/bash
if [ ! -f ./backup-image.out ]; then
	aws ec2 create-image --instance-id i-0244c1f6d357f785f --name "backupserver" --no-reboot > backup-image.out
fi
ami_id=`cat backup-image.out | python3 -c "import sys, json; print(json.load(sys.stdin)['ImageId'])"`
echo $ami_id
aws ec2 wait image-available --image-ids "$ami_id" 
if [ $? -eq 0 ]; then
	source .env/bin/activate
	pip install -r requirements.txt
	cdk synth
	#cdk deploy
	cdk diff
else
	echo Create Image failed. Cannot proceed to CDK deployment. 
fi

#!/usr/bin/env python3

from aws_cdk import core

from form_bookit.form_bookit_stack import FormBookitStack


app = core.App()
env_SG = core.Environment(account="695188266141", region="ap-southeast-1")
FormBookitStack(app, "form-bookit", env=env_SG)

app.synth()

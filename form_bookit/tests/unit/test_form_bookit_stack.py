import json
import pytest

from aws_cdk import core
from form_bookit.form_bookit_stack import FormBookitStack


def get_template():
    app = core.App()
    FormBookitStack(app, "form-bookit")
    return json.dumps(app.synth().get_stack("form-bookit").template)


def test_sqs_queue_created():
    assert("AWS::SQS::Queue" in get_template())


def test_sns_topic_created():
    assert("AWS::SNS::Topic" in get_template())

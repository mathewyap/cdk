# README #

This README would normally document whatever steps are necessary to get your application up and running.
Pre-requisites:
* AWS CLI
* CDK
* Python3 with pip3 and venv
* clone the required stack from Git

### Steps to use form_bookit stack ###
1. Configure the awscli to an account with admin permissions using aws config
2. cd form_bookit
3. Activate virtual env: source .env/bin/activate
4. Apply all required modules: pip install -r requirements
5. Synthesize relevant stack: cdk synth
6. Deploy relevant stack: cdk deploy
 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

